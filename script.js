document.getElementById('fileinput').addEventListener('change', readFileFromEvent, false)

function readFileFromEvent (event) {
	var file = event.target.files[0]
	var r = new FileReader()
	r.onload = updateContent
	r.readAsBinaryString(file)
}

function logFileLoad(event) {
	var content = event.target.result
	console.log(content)
}

function updateContent(event) {
	// logFileLoad(event)
	setContent(parseMarkDown(event.target.result))
}

function setContent(content) {
	document.getElementById("main").innerHTML = content
}

function parseMarkDown (content) {
	// Do your work here
	var contentSplit = content.split('\n\n').map(s => s.split(''))
	return contentSplit.map(procressText).join('')
}

function procressText (content) {
	var sharpCount = R.takeWhile(s => s == '#', content).length
	var text = R.dropWhile(s => s == '#' || s == ' ', content)
	if (sharpCount > 0) return headerTag(sharpCount, text)
	else return paragraphTag(text)
}

function headerTag(n, content) {
	return '<h' + n + '>' + content.join('') + '</h' + n +'>'
}

function paragraphTag(content) {
	return '<p>' + boldText(content,true) + '</p>'
}

function boldText(content, flag) {
	var tag = '</b>'
	if(flag) tag='<b>'

	var firstPart = R.takeWhile(s => s != '*', content).join('')
	var secoundPart = R.dropWhile(s => s =='*', R.dropWhile(s => s != '*', content))

	if ((secoundPart.length) > 0) {
	  return firstPart
		+ tag 
		+ boldText(secoundPart, !flag)
	} else {
	  return content.join('')
	}
  }